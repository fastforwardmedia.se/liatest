LIATEST - Fast Forward Media

Uppgiftens syfte är att vi skall se hur du har gått till väga för att lösa uppgiften.
Målet är att hinna så långt som möjligt på max 6 timmar och sedan committa in projektet även fast den inte är klar.
Vi vill att du beskriver dina tankar kring projektet här och committar in det tillsammans med en MYSQL-fil.

När du känner dit redo så kan du skriva en kommentar i någon av filerna och göra en commit. Då startar dina 6h.

UPPGIFTEN

Skapa en enkel chatt för webben där inloggade användare kan skriva meddelanden till varandra.
Chatten behöver inte reagera direkt utan det räcker om din ajaxfunktion gör ett request ungefär var 10 sekund.
Till detta skall du använda css eller sass (utan bootstrap eller liknande), jquery, php och mysqli eller pdo.
Som du ser så har jag påbörjat lite lätt men du får gärna stylea om den enligt design på liatest.pdf.
Ett tips, lägg tid på design sist och fukusera på funktionalitet i första hand.
Plus på kanten om du använder dig av templating för snyggare filstruktur samt håller isär logik och markup.

Du behöver inte skapa funktionen för att skapa nya användare men vi vill att man ska kunna logga in och starta en session.

Dessa användare med lösen kan du använda i din usertabell:

Anv: Tatsumi
Lös: lol123#

Anv: Calle
Lös: lol123#

Anv: Kalle
Lös: lol123#


TANKAR KRING PROJEKTET

1: Vilka delar av projektet var svårast och varför?

2: Vad tog längst tid att utföra?

3: Något i din kod som du är extra nöjd med?

4: Om du inte hann klart, hur lång tid skulle du tro att det är kvar tills en fungerande chatt?
