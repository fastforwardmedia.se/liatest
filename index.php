<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Liatest - Fast Forward Media</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" media="all" href="assets/style.css" />
    </head>
    <body>
        <section class="pagewrapper">
            <h1>Liatest - Chatten</h1>
            <div id="messagebox">
                <p>Chat messages goe's here..</p>
            </div>
            <!-- Show chat form if logged in -->
            <form name="insert_post" action="#" method="POST">
                <label for="message">
                    Skriv meddelande som [namn]:
                </label>
                <textarea id="message" name="message"></textarea>
                <button type="submit" name="submit_post">Skicka meddelande</button>
                <button type="submit" name="log_out">Logga ut</button>
            </form>
            <!-- Show log in form if not logged in -->
            <form name="log_in" action="#" method="POST">
                <label for="username">
                    Användarnamn:
                </label>
                <input type="text" id="username" name="username" value="" />
                <label for="password">
                    Lösenord:
                </label>
                <input type="password" id="password" name="password" value="" />
                <button type="submit" name="submit">Logga in</button>
            </form>
        </section>
        <script type="application/javascript" src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <script type="application/javascript" src="assets/script.js"></script>
    </body>
</html>

